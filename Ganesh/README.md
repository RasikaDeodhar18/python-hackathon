# README #

### What is this repository for? ###

* This folder includes the python notebook file in order to perform data analysis of house construction data available from the API published by Ireland government on their website "DATA.GOV.IE"
* Version: 1.0

### How do I get set up? ###

* Install python jupyter on your machine
* Make sure python interpreter has installed on your machine
* Install the mentioned libraries on your machine by command "pip install library_name" (library_name: urllib, csv, pandas, json, numpy, matplotlib, plotly)


### About python notebook code and execrice:

* Our folder contains python notebook file named "Citi_Week 4 training_Python data analysis.ipynb"
* In this notebook file, we are hitting 2 APIs published on the Ireland government website "https://data.gov.ie/dataset/" to retrieve the 'Social Housing Construction Status Report Q2 and Q3 019'
* In this exercise we are demonstarting the below task:
	1. Collect the housing data in the JSON format from 2 different APIs. Parse, store the data in appropriate data format and save the copy of data on local in the form of CSV files. 
	2. Integrate the data of 2 APIs into single dataframe for further analysis
	3. Perform some data processing activities
	4. Performing data analysis stuff
	5. Graphical representation of data for ease of understanding
	6. Drawing conclusion from these all execrice.