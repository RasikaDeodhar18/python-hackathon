import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pandas_datareader as pdr
import datetime as dt
import requests
import json
import time
import csv

from selenium import webdriver
from bs4 import BeautifulSoup



#Constants to use
PATH = "C:\Program Files (x86)\chromedriver.exe"
WEB_URL = 'http://neueda.conygre.com/Sites/EMEA+Tech+Grad+Program/index.html'
BLOOMBERG_URL = 'https://www.bloomberg.com/europe'
SLACK_WEBHOOK = 'https://hooks.slack.com/services/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'
SIMON_WEBHOOK = 'https://hooks.slack.com/services/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'

driver = webdriver.Chrome(PATH)

CODE = ['ERROR', 'RUNNING']


# Check if website is running
def check_website(url):

    try:

        response = requests.get(url, timeout = 5)

        if(response.status_code != 200):

            print(CODE[0] + response.status_code)
            
            
        else:

            print(url + ' : ' + CODE[1] + '\n' )
            

    except requests.exceptions.RequestException:

        print(CODE[0] + ' : Please check URL')



def send_slack_message(msg):
    response = requests.post(SIMON_WEBHOOK, json.dumps({"text": msg}))

    print("Request status code:", response.status_code)
    return response


def get_bloomberg_headlines():
    
    driver.get(BLOOMBERG_URL)

    print(driver.title)

    headlines = driver.find_elements_by_class_name("story-package-module__story__headline-link")

    message = 'Current Bloomberg headlines are: \n'

    for s in range(len(headlines)):

        # Write to CSV and print if headlines has changed

        # Compare to CSV to find differents
        if(s<=3):
            message += headlines[s].text

            message += ' '

            message += headlines[s].get_attribute('href')

            message += '\n'

    driver.quit()

    # send_slack_message(message)
    print(message)


if(__name__ == '__main__'):

    check_website(WEB_URL)

    get_bloomberg_headlines()


